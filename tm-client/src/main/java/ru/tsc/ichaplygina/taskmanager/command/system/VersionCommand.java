package ru.tsc.ichaplygina.taskmanager.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

@Component
public class VersionCommand extends AbstractCommand {

    @NotNull
    public final static String CMD_NAME = "version";

    @NotNull
    public final static String ARG_NAME = "-v";

    @NotNull
    public final static String DESCRIPTION = "show version info";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getArgument() {
        return ARG_NAME;
    }

    @Override
    public final void execute() {
        printLinesWithEmptyLine(Manifests.read("build"));
    }

}
