package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class TaskAddToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "add task to project";

    @NotNull
    public final static String DESCRIPTION = "add task to a project";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String projectId = readLine(PROJECT_ID_INPUT);
        @NotNull final String taskId = readLine(TASK_ID_INPUT);
        getTaskEndpoint().addTaskToProject(sessionService.getSession(), projectId, taskId);
    }

}
