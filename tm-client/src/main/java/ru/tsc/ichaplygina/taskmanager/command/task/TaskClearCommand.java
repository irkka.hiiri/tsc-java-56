package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "clear tasks";

    @NotNull
    public final static String DESCRIPTION = "delete all tasks";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        getTaskEndpoint().clearTasks(sessionService.getSession());
    }

}
