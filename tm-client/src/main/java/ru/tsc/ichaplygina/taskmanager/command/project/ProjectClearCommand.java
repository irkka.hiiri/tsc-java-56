package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "clear projects";

    @NotNull
    public final static String DESCRIPTION = "delete all projects";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        getProjectEndpoint().clearProjects(sessionService.getSession());
    }

}
