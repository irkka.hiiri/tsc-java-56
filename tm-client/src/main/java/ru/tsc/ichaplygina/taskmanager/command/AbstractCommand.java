package ru.tsc.ichaplygina.taskmanager.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.ISessionService;
import ru.tsc.ichaplygina.taskmanager.endpoint.AdminEndpoint;
import ru.tsc.ichaplygina.taskmanager.endpoint.SessionEndpoint;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Getter
@Setter
@Component
public abstract class AbstractCommand {

    @NotNull
    protected static final String ID_INPUT = "Please enter id: ";

    @NotNull
    protected static final String INDEX_INPUT = "Please enter index: ";

    @NotNull
    protected static final String NAME_INPUT = "Please enter name: ";

    @NotNull
    protected static final String DESCRIPTION_INPUT = "Please enter description: ";

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    protected ISessionService sessionService;

    @NotNull
    @Autowired
    protected AdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    public abstract void execute();

    @Nullable
    public abstract String getArgument();

    @NotNull
    public abstract String getCommand();

    @NotNull
    public abstract String getDescription();

    @Override
    public String toString() {
        return (getCommand() + (isEmptyString(getArgument()) ? "" : " [" + getArgument() + "]") + " - " + getDescription() + ".");
    }

}
