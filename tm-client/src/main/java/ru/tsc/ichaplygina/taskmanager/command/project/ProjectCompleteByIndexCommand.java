package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

@Component
public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "complete project by index";

    @NotNull
    public final static String DESCRIPTION = "complete project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        getProjectEndpoint().completeProjectByIndex(sessionService.getSession(), index);
    }

}
