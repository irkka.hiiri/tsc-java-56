package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class DomainSaveBase64Command extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "save base64";

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to base64 file";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().saveBase64(sessionService.getSession());
    }

}
