package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.ichaplygina.taskmanager.component.Bootstrap;
import ru.tsc.ichaplygina.taskmanager.configuration.ClientConfiguration;

public final class TaskManager {

    public static void main(String[] args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
