package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.endpoint.User;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class UserShowByLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "show user by login";

    @NotNull
    public static final String DESCRIPTION = "show user by login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final User user = getAdminEndpoint().findUserByLogin(sessionService.getSession(), login);
        showUser(user);
    }

}
