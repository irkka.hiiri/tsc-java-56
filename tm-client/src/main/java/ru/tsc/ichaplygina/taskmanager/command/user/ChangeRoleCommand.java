package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class ChangeRoleCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "change role";

    @NotNull
    public static final String DESCRIPTION = "change user's role";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String role = readLine(ENTER_ROLE);
        getAdminEndpoint().changeRole(sessionService.getSession(), login, role);
    }

}
