package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class DomainSaveXMLJaxbCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "save xml jaxb";

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to xml file using jaxb";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().saveXMLJaxb(sessionService.getSession());
    }

}
