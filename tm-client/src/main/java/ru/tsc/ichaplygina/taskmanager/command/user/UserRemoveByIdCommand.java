package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class UserRemoveByIdCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "remove user by id";

    @NotNull
    public static final String DESCRIPTION = "remove user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        getAdminEndpoint().removeUserById(sessionService.getSession(), id);
    }

}
