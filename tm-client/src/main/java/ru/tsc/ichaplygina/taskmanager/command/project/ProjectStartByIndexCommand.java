package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

@Component
public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "start project by index";

    @NotNull
    public final static String DESCRIPTION = "start project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        getProjectEndpoint().startProjectByIndex(sessionService.getSession(), index);
    }

}
