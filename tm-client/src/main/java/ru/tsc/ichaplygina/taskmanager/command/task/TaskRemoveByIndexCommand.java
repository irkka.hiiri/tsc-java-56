package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

@Component
public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "remove task by index";

    @NotNull
    public final static String DESCRIPTION = "remove task by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final int index = readNumber(INDEX_INPUT);
        getTaskEndpoint().removeTaskByIndex(sessionService.getSession(), index);
    }

}
