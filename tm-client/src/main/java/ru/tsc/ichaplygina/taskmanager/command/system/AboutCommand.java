package ru.tsc.ichaplygina.taskmanager.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

@Component
public final class AboutCommand extends AbstractCommand {

    @NotNull
    public final static String CMD_NAME = "about";

    @NotNull
    public final static String ARG_NAME = "-a";

    @NotNull
    public final static String DESCRIPTION = "show developer info";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getArgument() {
        return ARG_NAME;
    }

    @Override
    public final void execute() {
        printLinesWithEmptyLine(Manifests.read("developer"), Manifests.read("email"));
    }

}
