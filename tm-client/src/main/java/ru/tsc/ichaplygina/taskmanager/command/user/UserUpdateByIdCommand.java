package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class UserUpdateByIdCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "update user by id";

    @NotNull
    public static final String DESCRIPTION = "update user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String password = readLine(ENTER_PASSWORD);
        @NotNull final String email = readLine(ENTER_EMAIL);
        @NotNull final String role = readLine(ENTER_ROLE);
        @NotNull final String firstName = readLine(ENTER_FIRST_NAME);
        @NotNull final String middleName = readLine(ENTER_MIDDLE_NAME);
        @NotNull final String lastName = readLine(ENTER_LAST_NAME);
        getAdminEndpoint().updateUserById(sessionService.getSession(), id, login, password, email, role, firstName, middleName, lastName);
    }

}
