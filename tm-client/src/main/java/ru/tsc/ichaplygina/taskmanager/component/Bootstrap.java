package ru.tsc.ichaplygina.taskmanager.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.api.repository.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.api.service.ILogService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.CommandNotFoundException;
import ru.tsc.ichaplygina.taskmanager.repository.CommandRepository;
import ru.tsc.ichaplygina.taskmanager.util.SystemUtil;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Getter
@Setter
@Component
public final class Bootstrap {

    private static final boolean CONSOLE_LOG_ENABLED = true;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    private static final String PID_FILE_NAME = "task-manager.pid";

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    public void executeCommand(@NotNull final String commandName) {
        if (isEmptyString(commandName)) return;
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getCommands().get(commandName))
                .orElseThrow(() -> new CommandNotFoundException(commandName));
        execute(command);
    }

    public void executeCommandByArgument(@NotNull final String argument) {
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getArguments().get(argument))
                .orElseThrow(() -> new CommandNotFoundException(argument));
        execute(command);
    }

    public void execute(@NotNull final AbstractCommand command) {
        command.execute();
    }

    @SneakyThrows
    public void initCommands(@Nullable final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) return;
            @NotNull final String terminalCommand = command.getCommand();
            @Nullable final String commandArgument = command.getArgument();
            commandService.getCommands().put(terminalCommand, command);
            commandService.getArguments().put(commandArgument, command);
        }
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILE_NAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILE_NAME);
        file.deleteOnExit();
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void processInput() {
        logService.info("Program run in command-line mode.");
        showWelcome();
        @NotNull String command = readCommand();
        while (true) {
            try {
                if (isEmptyString(command)) {
                    command = readCommand();
                    continue;
                }
                logService.command("Executing command: " + command);
                executeCommand(command);
                System.out.println(APP_COMMAND_SUCCESS);
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.out.println(APP_COMMAND_ERROR);
            }
            command = readCommand();
        }
    }

    @NotNull
    private String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

    public void run(@NotNull final String... args) {
        initPID();
        initCommands(abstractCommands);
        fileScanner.init();
        if (args.length == 0) processInput();
        else
            try {
                logService.info("Program run in arguments mode.");
                executeCommandByArgument(args[0]);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
            }
    }

    private void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }

}
