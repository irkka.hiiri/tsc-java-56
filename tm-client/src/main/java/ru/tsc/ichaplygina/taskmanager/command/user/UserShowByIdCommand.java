package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.endpoint.User;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class UserShowByIdCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "show user by id";

    @NotNull
    public static final String DESCRIPTION = "show user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        @NotNull final User user = getAdminEndpoint().findUserById(sessionService.getSession(), id);
        showUser(user);
    }

}
