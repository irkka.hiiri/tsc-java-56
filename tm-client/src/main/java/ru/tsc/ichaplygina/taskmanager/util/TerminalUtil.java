package ru.tsc.ichaplygina.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Scanner;

@UtilityClass
public final class TerminalUtil {

    @NotNull
    private final static Scanner SCANNER = new Scanner(System.in);

    public static void printLinesWithEmptyLine(@Nullable final Object... lines) {
        if (lines == null) return;
        System.out.println();
        for (@Nullable final Object line : lines) System.out.println(line);
        System.out.println();
    }

    public static <T> void printList(@NotNull final List<T> list) {
        System.out.println();
        for (@NotNull final Object item : list) {
            System.out.println(item);
        }
        System.out.println();
    }

    @NotNull
    public static String readLine() {
        return SCANNER.nextLine().trim();
    }

    @NotNull
    public static String readLine(@NotNull final String output) {
        System.out.print(output);
        return readLine();
    }

    public static int readNumber() {
        try {
            return Integer.parseInt(readLine());
        } catch (@NotNull final NumberFormatException e) {
            return -1;
        }
    }

    public static int readNumber(@NotNull final String output) {
        System.out.print(output);
        return readNumber();
    }

}
