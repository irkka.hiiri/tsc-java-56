package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class DomainLoadJsonFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "load json fasterxml";

    @NotNull
    public final static String DESCRIPTION = "load projects, tasks and users from json file using fasterxml";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().loadJsonFasterXML(sessionService.getSession());
    }

}
