package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "show project by id";

    @NotNull
    public final static String DESCRIPTION = "show project by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        showProject(getProjectEndpoint().findProjectById(sessionService.getSession(), id));
    }

}
