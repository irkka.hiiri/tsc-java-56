package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class DomainLoadBase64Command extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "load base64";

    @NotNull
    public final static String DESCRIPTION = "load projects, tasks and users from base64 file";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().loadBase64(sessionService.getSession());
    }

}
