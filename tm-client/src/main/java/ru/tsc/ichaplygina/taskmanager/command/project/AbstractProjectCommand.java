package ru.tsc.ichaplygina.taskmanager.command.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.endpoint.Project;
import ru.tsc.ichaplygina.taskmanager.endpoint.ProjectEndpoint;

@Getter
@Setter
@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showProject(@NotNull final Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Created: " + project.getCreated());
        System.out.println("Status: " + project.getStatus());
        System.out.println("Started: " + project.getDateStart());
        System.out.println("Finished: " + project.getDateFinish());
        System.out.println("User Id: " + project.getUser().getId());
    }

}
