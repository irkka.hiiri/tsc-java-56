package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class DomainLoadBinaryCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "load binary";

    @NotNull
    public final static String DESCRIPTION = "load projects, tasks and users from binary file";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().loadBinary(sessionService.getSession());
    }

}
