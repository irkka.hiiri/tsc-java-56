package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class TaskShowByNameCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "show task by name";

    @NotNull
    public final static String DESCRIPTION = "show task by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        showTask(getTaskEndpoint().findTaskByName(sessionService.getSession(), name));
    }

}
