package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public final class LogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "logout from the system";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        getSessionEndpoint().closeSession(sessionService.getSession());
        sessionService.setSession(null);
    }
}
