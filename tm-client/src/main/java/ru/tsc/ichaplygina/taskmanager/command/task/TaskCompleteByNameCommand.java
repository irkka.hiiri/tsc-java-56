package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class TaskCompleteByNameCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "complete task by name";

    @NotNull
    public final static String DESCRIPTION = "complete task by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        getTaskEndpoint().completeTaskByName(sessionService.getSession(), name);
    }

}
