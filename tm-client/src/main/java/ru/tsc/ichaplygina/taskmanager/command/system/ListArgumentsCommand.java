package ru.tsc.ichaplygina.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

@Component
public final class ListArgumentsCommand extends AbstractCommand {

    @NotNull
    public final static String CMD_NAME = "list commands";

    @NotNull
    public final static String DESCRIPTION = "list available commands";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public final String getArgument() {
        return null;
    }

    @Override
    public final void execute() {
        System.out.println();
        for (@NotNull final AbstractCommand command : getCommandService().getCommandList()) {
            System.out.println(command.getCommand());
        }
        System.out.println();
    }

}
