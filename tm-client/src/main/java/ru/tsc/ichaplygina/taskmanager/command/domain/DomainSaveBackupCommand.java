package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class DomainSaveBackupCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "save backup";

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to backup xml file " +
            "which is loaded on application start. Runs automatically and can be run manually.";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().saveBackup(sessionService.getSession());
    }

}
