package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class DomainSaveBinaryCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "save binary";

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to binary file";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().saveBinary(sessionService.getSession());
    }

}
