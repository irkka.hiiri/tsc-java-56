package ru.tsc.ichaplygina.taskmanager.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.property.IApplicationProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.INetworkProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.IPasswordProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.ISessionProperty;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

@Service
public final class PropertyService implements IPropertyService, IPasswordProperty, IApplicationProperty, ISessionProperty, INetworkProperty {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_PROPERTY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_JAVA_OPTION = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_ENVIRONMENT_VARIABLE = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT_VALUE = "";

    @NotNull
    private static final String PASSWORD_ITERATION_PROPERTY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_JAVA_OPTION = "PASSWORD_ITERATION";

    @NotNull
    private static final String PASSWORD_ITERATION_ENVIRONMENT_VARIABLE = "PASSWORD_ITERATION";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT_VALUE = "0";

    @NotNull
    private static final String APP_VERSION_PROPERTY = "application.version";

    @NotNull
    private static final String APP_VERSION_JAVA_OPTION = "APPLICATION_VERSION";

    @NotNull
    private static final String APP_VERSION_ENVIRONMENT_VARIABLE = "APPLICATION_VERSION";

    @NotNull
    private static final String APP_VERSION_DEFAULT_VALUE = "";

    @NotNull
    private static final String AUTOSAVE_FREQUENCY_PROPERTY = "autosave.frequency";

    @NotNull
    private static final String AUTOSAVE_FREQUENCY_JAVA_OPTION = "AUTOSAVE_FREQUENCY";

    @NotNull
    private static final String AUTOSAVE_FREQUENCY_ENVIRONMENT_VARIABLE = "AUTOSAVE_FREQUENCY";

    @NotNull
    private static final String AUTOSAVE_FREQUENCY_DEFAULT_VALUE = "30000";

    @NotNull
    private static final String AUTOSAVE_ON_EXIT_PROPERTY = "autosave.onExit";

    @NotNull
    private static final String AUTOSAVE_ON_EXIT_JAVA_OPTION = "AUTOSAVE_ON_EXIT";

    @NotNull
    private static final String AUTOSAVE_ON_EXIT_ENVIRONMENT_VARIABLE = "AUTOSAVE_ON_EXIT";

    @NotNull
    private static final String AUTOSAVE_ON_EXIT_DEFAULT_VALUE = "true";

    @NotNull
    private static final String FILE_SCANNER_FREQUENCY_PROPERTY = "fileScanner.frequency";

    @NotNull
    private static final String FILE_SCANNER_FREQUENCY_JAVA_OPTION = "FILE_SCANNER_FREQUENCY";

    @NotNull
    private static final String FILE_SCANNER_FREQUENCY_ENVIRONMENT_VARIABLE = "FILE_SCANNER_FREQUENCY";

    @NotNull
    private static final String FILE_SCANNER_FREQUENCY_DEFAULT_VALUE = "5000";

    @NotNull
    private static final String FILE_SCANNER_PATH_PROPERTY = "fileScanner.path";

    @NotNull
    private static final String FILE_SCANNER_PATH_JAVA_OPTION = "FILE_SCANNER_PATH";

    @NotNull
    private static final String FILE_SCANNER_PATH_ENVIRONMENT_VARIABLE = "FILE_SCANNER_PATH";

    @NotNull
    private static final String FILE_SCANNER_PATH_DEFAULT_VALUE = "./";

    @NotNull
    private static final String SIGN_SECRET_PROPERTY = "session.sign.secret";

    @NotNull
    private static final String SIGN_SECRET_JAVA_OPTION = "SESSION_SIGN_SECRET";

    @NotNull
    private static final String SIGN_SECRET_ENVIRONMENT_VARIABLE = "SESSION_SIGN_SECRET";

    @NotNull
    private static final String SIGN_SECRET_DEFAULT_VALUE = "";

    @NotNull
    private static final String SIGN_ITERATION_PROPERTY = "session.sign.iteration";

    @NotNull
    private static final String SIGN_ITERATION_JAVA_OPTION = "SESSION_SIGN_ITERATION";

    @NotNull
    private static final String SIGN_ITERATION_ENVIRONMENT_VARIABLE = "SESSION_SIGN_ITERATION";

    @NotNull
    private static final String SIGN_ITERATION_DEFAULT_VALUE = "0";

    @NotNull
    private static final String APP_SERVER_PROPERTY = "application.server";

    @NotNull
    private static final String APP_SERVER_JAVA_OPTION = "APPLICATION_SERVER";

    @NotNull
    private static final String APP_SERVER_ENVIRONMENT_VARIABLE = "APPLICATION_SERVER";

    @NotNull
    private static final String APP_SERVER_DEFAULT_VALUE = "http://localhost";

    @NotNull
    private static final String APP_PORT_PROPERTY = "application.port";

    @NotNull
    private static final String APP_PORT_JAVA_OPTION = "APPLICATION_PORT";

    @NotNull
    private static final String APP_PORT_ENVIRONMENT_VARIABLE = "APPLICATION_PORT";

    @NotNull
    private static final String APP_PORT_DEFAULT_VALUE = "8080";

    @NotNull
    private final Properties properties = new Properties();

    public PropertyService() {
        loadPropertiesFromFile();
    }

    @SneakyThrows
    private void loadPropertiesFromFile() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getValue(@NotNull final String nameJavaOpts, @NotNull final String nameEnvironmentVariable,
                            @NotNull final String nameProperty, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(nameJavaOpts)) return System.getProperty(nameJavaOpts);
        if (System.getenv().containsKey(nameEnvironmentVariable)) return System.getenv(nameEnvironmentVariable);
        if (properties.containsKey(nameProperty)) return properties.getProperty(nameProperty);
        return defaultValue;
    }

    @NotNull
    private Integer getValueInteger(@NotNull final String nameJavaOpts, @NotNull final String nameEnvironmentVariable,
                                    @NotNull final String nameProperty, @NotNull final String defaultValue) {
        @NotNull final String value = getValue(nameJavaOpts, nameEnvironmentVariable, nameProperty, defaultValue);
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final NumberFormatException e) {
            return Integer.parseInt(PASSWORD_ITERATION_DEFAULT_VALUE);
        }
    }

    @NotNull
    private Boolean getValueBoolean(@NotNull final String nameJavaOpts, @NotNull final String nameEnvironmentVariable,
                                    @NotNull final String nameProperty, @NotNull final String defaultValue) {
        @NotNull final String value = getValue(nameJavaOpts, nameEnvironmentVariable, nameProperty, defaultValue);
        return Boolean.parseBoolean(value);
    }


    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValue(APP_VERSION_JAVA_OPTION, APP_VERSION_ENVIRONMENT_VARIABLE, APP_VERSION_PROPERTY, APP_VERSION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInteger(PASSWORD_ITERATION_JAVA_OPTION,
                PASSWORD_ITERATION_ENVIRONMENT_VARIABLE,
                PASSWORD_ITERATION_PROPERTY,
                PASSWORD_ITERATION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_JAVA_OPTION,
                PASSWORD_SECRET_ENVIRONMENT_VARIABLE,
                PASSWORD_SECRET_PROPERTY,
                PASSWORD_SECRET_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getAutosaveFrequency() {
        return getValueInteger(AUTOSAVE_FREQUENCY_JAVA_OPTION,
                AUTOSAVE_FREQUENCY_ENVIRONMENT_VARIABLE,
                AUTOSAVE_FREQUENCY_PROPERTY,
                AUTOSAVE_FREQUENCY_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Boolean isAutosaveOnExit() {
        return getValueBoolean(AUTOSAVE_ON_EXIT_JAVA_OPTION,
                AUTOSAVE_ON_EXIT_ENVIRONMENT_VARIABLE,
                AUTOSAVE_ON_EXIT_PROPERTY,
                AUTOSAVE_ON_EXIT_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getFileScannerFrequency() {
        return getValueInteger(FILE_SCANNER_FREQUENCY_JAVA_OPTION,
                FILE_SCANNER_FREQUENCY_ENVIRONMENT_VARIABLE,
                FILE_SCANNER_FREQUENCY_PROPERTY,
                FILE_SCANNER_FREQUENCY_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getFileScannerPath() {
        return getValue(FILE_SCANNER_PATH_JAVA_OPTION,
                FILE_SCANNER_PATH_ENVIRONMENT_VARIABLE,
                FILE_SCANNER_PATH_PROPERTY,
                FILE_SCANNER_PATH_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getSignIteration() {
        return getValueInteger(SIGN_ITERATION_JAVA_OPTION,
                SIGN_ITERATION_ENVIRONMENT_VARIABLE,
                SIGN_ITERATION_PROPERTY,
                SIGN_ITERATION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getSignSecret() {
        return getValue(SIGN_SECRET_JAVA_OPTION,
                SIGN_SECRET_ENVIRONMENT_VARIABLE,
                SIGN_SECRET_PROPERTY,
                SIGN_SECRET_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getServer() {
        return getValue(APP_SERVER_JAVA_OPTION,
                APP_SERVER_ENVIRONMENT_VARIABLE,
                APP_SERVER_PROPERTY,
                APP_SERVER_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getPort() {
        return getValue(APP_PORT_JAVA_OPTION,
                APP_PORT_ENVIRONMENT_VARIABLE,
                APP_PORT_PROPERTY,
                APP_PORT_DEFAULT_VALUE);
    }

}
