package ru.tsc.ichaplygina.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Component
public final class ListCommandsCommand extends AbstractCommand {

    @NotNull
    public final static String CMD_NAME = "list arguments";

    @NotNull
    public final static String DESCRIPTION = "list available command-line arguments";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public final String getArgument() {
        return null;
    }

    @Override
    public final void execute() {
        System.out.println();
        for (@NotNull final AbstractCommand command : getCommandService().getCommandList()) {
            if (isEmptyString(command.getArgument())) continue;
            System.out.println(command.getArgument());
        }
        System.out.println();
    }

}
