package ru.tsc.ichaplygina.taskmanager.command.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.endpoint.Task;
import ru.tsc.ichaplygina.taskmanager.endpoint.TaskEndpoint;

@Getter
@Setter
@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public static final String TASK_ID_INPUT = "Please enter task id: ";

    @NotNull
    public static final String PROJECT_ID_INPUT = "Please enter project id: ";

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showTask(@NotNull final Task task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Created: " + task.getCreated());
        System.out.println("Status: " + task.getStatus());
        System.out.println("Started: " + task.getDateStart());
        System.out.println("Finished: " + task.getDateFinish());
        System.out.println("User Id: " + task.getUser().getId());
    }

}
