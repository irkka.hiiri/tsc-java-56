package ru.tsc.ichaplygina.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.api.repository.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    public final Map<String, AbstractCommand> getArguments() {
        return arguments;
    }

    @NotNull
    public final Map<String, AbstractCommand> getCommands() {
        return commands;
    }
}
