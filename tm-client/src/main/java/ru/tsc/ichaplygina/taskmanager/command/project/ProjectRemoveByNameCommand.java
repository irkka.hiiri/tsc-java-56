package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "remove project by name";

    @NotNull
    public final static String DESCRIPTION = "remove project by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        getProjectEndpoint().removeProjectByName(sessionService.getSession(), name);
    }

}
