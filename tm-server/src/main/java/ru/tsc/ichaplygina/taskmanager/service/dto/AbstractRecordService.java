package ru.tsc.ichaplygina.taskmanager.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IAbstractRecordService;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRecordService<E extends AbstractModelDTO> implements IAbstractRecordService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    public abstract void add(@NotNull final E entity);

    @Override
    public abstract void addAll(@Nullable List<E> entities);

    @Override
    public abstract void clear();

    @NotNull
    @Override
    public abstract List<E> findAll();

    @Nullable
    @Override
    public abstract E findById(@NotNull final String id);

    @Override
    public abstract long getSize();

    @Override
    public abstract boolean isEmpty();

    @Nullable
    @Override
    public abstract E removeById(@NotNull final String id);

}
