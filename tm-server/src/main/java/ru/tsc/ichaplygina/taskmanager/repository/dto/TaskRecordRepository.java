package ru.tsc.ichaplygina.taskmanager.repository.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.ITaskRecordRepository;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class TaskRecordRepository extends AbstractBusinessEntityRecordRepository<TaskDTO> implements ITaskRecordRepository {

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM TaskDTO").executeUpdate();
    }

    @Override
    public void clearForUser(@NotNull String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return entityManager.createQuery("FROM TaskDTO", TaskDTO.class).getResultList();
    }

    @Override
    public @NotNull List<TaskDTO> findAllByProjectId(@NotNull String projectId) {
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.projectId = :projectId", TaskDTO.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public @NotNull List<TaskDTO> findAllByProjectIdForUser(@NotNull String userId, @NotNull String projectId) {
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.projectId = :projectId e.userId = :userId", TaskDTO.class)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllForUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable TaskDTO findById(@NotNull String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public @Nullable TaskDTO findByIdForUser(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("FROM TaskDTO e WHERE e.id = :id AND e.userId = :userId", TaskDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable TaskDTO findByIndex(int index) {
        return entityManager
                .createQuery("FROM TaskDTO", TaskDTO.class)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable TaskDTO findByIndexForUser(@NotNull String userId, int index) {
        return entityManager
                .createQuery("FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable TaskDTO findByName(@NotNull String name) {
        return entityManager
                .createQuery("FROM TaskDTO e WHERE e.name = :name", TaskDTO.class)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable TaskDTO findByNameForUser(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable TaskDTO findTaskInProject(@NotNull String taskId, @NotNull String projectId) {
        return entityManager
                .createQuery("FROM TaskDTO e WHERE e.id = :taskId AND e.projectId = :projectId", TaskDTO.class)
                .setParameter("taskId", taskId)
                .setParameter("projectId", projectId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable TaskDTO findTaskInProjectForUser(@NotNull String userId, @NotNull String taskId, @NotNull String projectId) {
        return entityManager
                .createQuery("FROM TaskDTO e WHERE e.id = :taskId AND e.projectId = :projectId AND e.userId = :userId", TaskDTO.class)
                .setParameter("taskId", taskId)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByIndex(int index) {
        return entityManager
                .createQuery("SELECT id FROM TaskDTO", String.class)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByIndexForUser(@NotNull String userId, int index) {
        return entityManager
                .createQuery("SELECT id FROM TaskDTO e WHERE e.userId = :userId", String.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByName(@NotNull String name) {
        return entityManager
                .createQuery("SELECT id FROM TaskDTO e WHERE e.name = :name", String.class)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByNameForUser(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("SELECT id FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", String.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDTO e", Long.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSizeForUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeAllByProjectId(@NotNull String projectId) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.projectId = :projectId")
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(entityManager.getReference(TaskDTO.class, id));
    }

    @Override
    public void removeByIdForUser(@NotNull String userId, @NotNull String id) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.id = :id AND e.userId = :userId")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(int index) {
        entityManager
                .createQuery("DELETE FROM TaskDTO")
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public void removeByIndexForUser(@NotNull String userId, int index) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public void removeByName(@NotNull String name) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.name = :name")
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByNameForUser(@NotNull String userId, @NotNull String name) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("name", name)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
