package ru.tsc.ichaplygina.taskmanager.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public interface ITaskService extends IAbstractBusinessEntityService<Task> {

    @Nullable Task addTaskToProject(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

    @NotNull List<Task> findAllByProjectId(@NotNull String userId,
                                           @NotNull String projectId,
                                           @Nullable String sortBy);

    void removeAllByProjectId(String projectId);

    @Nullable Task removeTaskFromProject(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);
}
