package ru.tsc.ichaplygina.taskmanager.repository.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.IProjectRecordRepository;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class ProjectRecordRepository extends AbstractBusinessEntityRecordRepository<ProjectDTO> implements IProjectRecordRepository {

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM ProjectDTO").executeUpdate();
    }

    @Override
    public void clearForUser(@NotNull String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @NotNull List<ProjectDTO> findAll() {
        return entityManager.createQuery("FROM ProjectDTO", ProjectDTO.class).getResultList();
    }

    @Override
    public @NotNull List<ProjectDTO> findAllForUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable ProjectDTO findById(@NotNull String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public @Nullable ProjectDTO findByIdForUser(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("FROM ProjectDTO e WHERE e.id = :id AND e.userId = :userId", ProjectDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable ProjectDTO findByIndex(int index) {
        return entityManager
                .createQuery("FROM ProjectDTO", ProjectDTO.class)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable ProjectDTO findByIndexForUser(@NotNull String userId, int index) {
        return entityManager
                .createQuery("FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable ProjectDTO findByName(@NotNull String name) {
        return entityManager
                .createQuery("FROM ProjectDTO e WHERE e.name = :name", ProjectDTO.class)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable ProjectDTO findByNameForUser(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByIndex(int index) {
        return entityManager
                .createQuery("SELECT id FROM ProjectDTO", String.class)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByIndexForUser(@NotNull String userId, int index) {
        return entityManager
                .createQuery("SELECT id FROM ProjectDTO e WHERE e.userId = :userId", String.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByName(@NotNull String name) {
        return entityManager
                .createQuery("SELECT id FROM ProjectDTO e WHERE e.name = :name", String.class)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByNameForUser(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("SELECT id FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name", String.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e", Long.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSizeForUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(entityManager.getReference(ProjectDTO.class, id));
    }

    @Override
    public void removeByIdForUser(@NotNull String userId, @NotNull String id) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.id = :id AND e.userId = :userId")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(int index) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO")
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public void removeByIndexForUser(@NotNull String userId, int index) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public void removeByName(@NotNull String name) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.name = :name")
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByNameForUser(@NotNull String userId, @NotNull String name) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("name", name)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
