package ru.tsc.ichaplygina.taskmanager.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;

import java.util.List;

public interface ITaskRecordService extends IAbstractBusinessEntityRecordService<TaskDTO> {

    @Nullable TaskDTO addTaskToProject(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

    @NotNull List<TaskDTO> findAllByProjectId(@NotNull String userId,
                                              @NotNull String projectId,
                                              @Nullable String sortBy);

    void removeAllByProjectId(String projectId);

    @Nullable TaskDTO removeTaskFromProject(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);
}
