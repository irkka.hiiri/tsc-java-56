package ru.tsc.ichaplygina.taskmanager.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Session;

public interface ISessionService extends IAbstractService<Session> {

    void closeSession(@NotNull Session session);

    Session openSession(@NotNull String login, @NotNull String password);

    void validatePrivileges(@NotNull String userId);

    void validateSession(@Nullable Session session);
}
