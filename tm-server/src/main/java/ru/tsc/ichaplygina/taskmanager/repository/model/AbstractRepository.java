package ru.tsc.ichaplygina.taskmanager.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.IAbstractRepository;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository<E extends AbstractModel> implements IAbstractRepository<E> {

    @NotNull
    @Autowired
    public EntityManager entityManager;

    @Override
    public void add(@NotNull E entity) {
        entityManager.persist(entity);
    }

    @Override
    public abstract void clear();

    @NotNull
    @Override
    public abstract List<E> findAll();

    @Nullable
    @Override
    public abstract E findById(@NotNull String id);

    @Override
    public abstract long getSize();

    @Override
    public abstract void removeById(@NotNull final String id);

    @Override
    public void update(@NotNull E entity) {
        entityManager.merge(entity);
    }

}
