package ru.tsc.ichaplygina.taskmanager.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractBusinessEntityDTO;

import java.util.List;

public interface IAbstractBusinessEntityRecordService<E extends AbstractBusinessEntityDTO> extends IAbstractRecordService<E> {

    void add(@NotNull String userId, @NotNull String entityName, @Nullable String entityDescription);

    void clear(String userId);

    @Nullable E completeById(@NotNull String userId,
                             @NotNull String projectId);

    @Nullable E completeByIndex(@NotNull String userId, int projectIndex);

    @Nullable E completeByName(@NotNull String userId, @NotNull String projectName);

    @NotNull List<E> findAll(@NotNull String userId);

    @NotNull List<E> findAll(@NotNull String userId, @NotNull String sortBy);

    @Nullable E findById(@NotNull String userId, @Nullable String entityId);

    @Nullable E findByIndex(@NotNull String userId, int entityIndex);

    @Nullable E findByName(@NotNull String userId, @NotNull String entityName);

    @Nullable String getId(@NotNull String userId, @NotNull String entityName);

    @Nullable String getId(@NotNull String userId, int index);

    long getSize(@NotNull String userId);

    boolean isEmpty(@NotNull String userId);

    @Nullable E removeById(@NotNull String userId, @NotNull String projectId);

    @Nullable E removeByIndex(@NotNull String userId, int projectIndex);

    @Nullable E removeByName(@NotNull String userId, @NotNull String entityName);

    @Nullable E startById(@NotNull String userId, @NotNull String projectId);

    @Nullable E startByIndex(@NotNull String userId, int projectIndex);

    @Nullable E startByName(@NotNull String userId, @NotNull String projectName);

    @Nullable E updateById(@NotNull String userId,
                           @NotNull String entityId,
                           @NotNull String entityName,
                           @Nullable String entityDescription);

    @Nullable
    E updateByIndex(@NotNull String userId,
                    int entityIndex,
                    @NotNull String entityName,
                    @Nullable String entityDescription);

}
