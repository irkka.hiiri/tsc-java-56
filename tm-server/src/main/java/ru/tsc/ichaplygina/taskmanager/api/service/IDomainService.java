package ru.tsc.ichaplygina.taskmanager.api.service;

public interface IDomainService {

    void loadBackup();

    void loadBase64();

    void loadBinary();

    void loadJsonFasterXML();

    void loadJsonJaxb();

    void loadXMLFasterXML();

    void loadXMLJaxb();

    void loadYAMLFasterXML();

    void saveBackup();

    void saveBase64();

    void saveBinary();

    void saveJsonFasterXML();

    void saveJsonJaxb();

    void saveXMLFasterXML();

    void saveXMLJaxb();

    void saveYAMLFasterXML();
}
