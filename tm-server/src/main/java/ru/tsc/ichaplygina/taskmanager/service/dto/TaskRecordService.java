package ru.tsc.ichaplygina.taskmanager.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.ITaskRecordRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.ITaskRecordService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IUserRecordService;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskRecordService extends AbstractBusinessEntityRecordService<TaskDTO> implements ITaskRecordService {

    @NotNull
    @Autowired
    private IUserRecordService userService;

    @NotNull
    public ITaskRecordRepository getRepository() {
        return context.getBean(ITaskRecordRepository.class);
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final TaskDTO task) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO(name, description, userId);
        add(task);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<TaskDTO> taskList) {
        if (taskList == null) return;
        for (final TaskDTO task : taskList) add(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public final TaskDTO addTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final TaskDTO task = userService.isPrivilegedUser(userId) ?
                    repository.findById(taskId) : repository.findByIdForUser(userId, taskId);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            task.setProjectId(projectId);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.clear();
            else repository.clearForUser(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO completeById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.COMPLETED);
    }

    @Nullable
    @Override
    public TaskDTO completeByIndex(@NotNull final String userId, final int index) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByIndex(index) :
                    repository.getIdByIndexForUser(userId, index);
            return completeById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByName(name) :
                    repository.getIdByNameForUser(userId, name);
            if (id == null) return null;
            return completeById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@NotNull final String userId) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findAll() : repository.findAllForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<TaskDTO> comparator = getComparator(sortBy);
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                    repository.findAllForUser(userId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public final List<TaskDTO> findAllByProjectId(@NotNull final String userId,
                                                  @NotNull final String projectId,
                                                  @Nullable final String sortBy) {
        @NotNull final Comparator<TaskDTO> comparator = getComparator(sortBy);
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findAllByProjectId(projectId).stream().sorted(comparator).collect(Collectors.toList()) :
                    repository.findAllByProjectIdForUser(userId, projectId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findById(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findById(taskId) : repository.findByIdForUser(userId, taskId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findByIndex(entityIndex) : repository.findByIndexForUser(userId, entityIndex);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findByName(entityName) : repository.findByNameForUser(userId, entityName);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.getIdByIndex(index) : repository.getIdByIndexForUser(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.getIdByName(entityName) : repository.getIdByNameForUser(userId, entityName);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@NotNull final String userId) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.getSize() : repository.getSizeForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Override
    @SneakyThrows
    public final void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final TaskDTO task = findById(id);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final TaskDTO task = findById(userId, id);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.removeById(id);
            else repository.removeByIdForUser(userId, id);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final TaskDTO task = findByIndex(userId, index);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.removeByIndex(index);
            else repository.removeByIndexForUser(userId, index);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final TaskDTO task = findByName(userId, name);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.removeByName(name);
            else repository.removeByNameForUser(userId, name);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final TaskDTO removeTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final TaskDTO task = userService.isPrivilegedUser(userId) ?
                    repository.findTaskInProject(taskId, projectId) : repository.findTaskInProjectForUser(userId, taskId, projectId);
            Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO startById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public TaskDTO startByIndex(@NotNull final String userId, final int index) {
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByIndex(index) :
                    repository.getIdByIndexForUser(userId, index);
            return startById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO startByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByName(name) :
                    repository.getIdByNameForUser(userId, name);
            return startById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO updateById(@NotNull final String userId,
                              @NotNull final String taskId,
                              @NotNull final String taskName,
                              @Nullable final String taskDescription) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        if (isEmptyString(taskName)) throw new NameEmptyException();
        @Nullable final TaskDTO task = findById(userId, taskId);
        if (task == null) return null;
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            task.setName(taskName);
            task.setDescription(taskDescription);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO updateByIndex(@NotNull final String userId,
                                 final int entityIndex,
                                 @NotNull final String entityName,
                                 @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    private TaskDTO updateStatus(@NotNull final String userId, @Nullable final String taskId, @NotNull final Status status) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        @Nullable final TaskDTO task = findById(userId, taskId);
        if (task == null) return null;
        @NotNull final ITaskRecordRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            task.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
