package ru.tsc.ichaplygina.taskmanager.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.ITaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskService extends AbstractBusinessEntityService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    public ITaskRepository getRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final Task task) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final User user = Optional.ofNullable(userService.findById(userId)).orElseThrow(UserNotFoundException::new);
        @NotNull final Task task = new Task(name, description, user);
        add(task);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<Task> taskList) {
        if (taskList == null) return;
        for (final Task task : taskList) add(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public final Task addTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final Task task = userService.isPrivilegedUser(userId) ?
                    repository.findById(taskId) : repository.findByIdForUser(userId, taskId);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            @Nullable final Project project = Optional.ofNullable(projectService.findById(userId, projectId)).orElseThrow(ProjectNotFoundException::new);
            task.setProject(project);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.clear();
            else repository.clearForUser(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task completeById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.COMPLETED);
    }

    @Nullable
    @Override
    public Task completeByIndex(@NotNull final String userId, final int index) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByIndex(index) :
                    repository.getIdByIndexForUser(userId, index);
            return completeById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByName(name) :
                    repository.getIdByNameForUser(userId, name);
            if (id == null) return null;
            return completeById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findAll() : repository.findAllForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<Task> comparator = getComparator(sortBy);
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                    repository.findAllForUser(userId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public final List<Task> findAllByProjectId(@NotNull final String userId,
                                               @NotNull final String projectId,
                                               @Nullable final String sortBy) {
        @NotNull final Comparator<Task> comparator = getComparator(sortBy);
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findAllByProjectId(projectId).stream().sorted(comparator).collect(Collectors.toList()) :
                    repository.findAllByProjectIdForUser(userId, projectId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findById(taskId) : repository.findByIdForUser(userId, taskId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findByIndex(entityIndex) : repository.findByIndexForUser(userId, entityIndex);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.findByName(entityName) : repository.findByNameForUser(userId, entityName);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.getIdByIndex(index) : repository.getIdByIndexForUser(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.getIdByName(entityName) : repository.getIdByNameForUser(userId, entityName);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@NotNull final String userId) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return userService.isPrivilegedUser(userId) ?
                    repository.getSize() : repository.getSizeForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Override
    @SneakyThrows
    public final void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final Task task = findById(id);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final Task task = findById(userId, id);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.removeById(id);
            else repository.removeByIdForUser(userId, id);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final Task task = findByIndex(userId, index);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.removeByIndex(index);
            else repository.removeByIndexForUser(userId, index);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final Task task = findByName(userId, name);
            if (task == null) return null;
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.removeByName(name);
            else repository.removeByNameForUser(userId, name);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final Task removeTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final Task task = userService.isPrivilegedUser(userId) ?
                    repository.findTaskInProject(taskId, projectId) : repository.findTaskInProjectForUser(userId, taskId, projectId);
            Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
            task.setProject(null);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task startById(@NotNull final String userId, @Nullable final String taskId) {
        return updateStatus(userId, taskId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public Task startByIndex(@NotNull final String userId, final int index) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByIndex(index) :
                    repository.getIdByIndexForUser(userId, index);
            return startById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        if (isEmptyString(name)) throw new NameEmptyException();
        try {
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByName(name) :
                    repository.getIdByNameForUser(userId, name);
            return startById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task updateById(@NotNull final String userId,
                           @NotNull final String taskId,
                           @NotNull final String taskName,
                           @Nullable final String taskDescription) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        if (isEmptyString(taskName)) throw new NameEmptyException();
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return null;
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            task.setName(taskName);
            task.setDescription(taskDescription);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task updateByIndex(@NotNull final String userId,
                              final int entityIndex,
                              @NotNull final String entityName,
                              @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    private Task updateStatus(@NotNull final String userId, @Nullable final String taskId, @NotNull final Status status) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return null;
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            task.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
