package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String APP_PORT_DEFAULT_VALUE = "8080";

    @NotNull
    private static final String APP_PORT_ENVIRONMENT_VARIABLE = "APPLICATION_PORT";

    @NotNull
    private static final String APP_PORT_JAVA_OPTION = "APPLICATION_PORT";

    @NotNull
    private static final String APP_PORT_PROPERTY = "application.port";

    @NotNull
    private static final String APP_SERVER_DEFAULT_VALUE = "http://localhost";

    @NotNull
    private static final String APP_SERVER_ENVIRONMENT_VARIABLE = "APPLICATION_SERVER";

    @NotNull
    private static final String APP_SERVER_JAVA_OPTION = "APPLICATION_SERVER";

    @NotNull
    private static final String APP_SERVER_PROPERTY = "application.server";

    @NotNull
    private static final String APP_VERSION_DEFAULT_VALUE = "";

    @NotNull
    private static final String APP_VERSION_ENVIRONMENT_VARIABLE = "APPLICATION_VERSION";

    @NotNull
    private static final String APP_VERSION_JAVA_OPTION = "APPLICATION_VERSION";

    @NotNull
    private static final String APP_VERSION_PROPERTY = "application.version";

    @NotNull
    private static final String AUTOSAVE_FREQUENCY_DEFAULT_VALUE = "30000";

    @NotNull
    private static final String AUTOSAVE_FREQUENCY_ENVIRONMENT_VARIABLE = "AUTOSAVE_FREQUENCY";

    @NotNull
    private static final String AUTOSAVE_FREQUENCY_JAVA_OPTION = "AUTOSAVE_FREQUENCY";

    @NotNull
    private static final String AUTOSAVE_FREQUENCY_PROPERTY = "autosave.frequency";

    @NotNull
    private static final String AUTOSAVE_ON_EXIT_DEFAULT_VALUE = "true";

    @NotNull
    private static final String AUTOSAVE_ON_EXIT_ENVIRONMENT_VARIABLE = "AUTOSAVE_ON_EXIT";

    @NotNull
    private static final String AUTOSAVE_ON_EXIT_JAVA_OPTION = "AUTOSAVE_ON_EXIT";

    @NotNull
    private static final String AUTOSAVE_ON_EXIT_PROPERTY = "autosave.onExit";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT_VALUE = "org.postgresql.Driver";

    @NotNull
    private static final String DATABASE_DRIVER_ENVIRONMENT_VARIABLE = "DATABASE_DRIVER";

    @NotNull
    private static final String DATABASE_DRIVER_JAVA_OPTION = "DATABASE_DRIVER";

    @NotNull
    private static final String DATABASE_DRIVER_PROPERTY = "database.driver";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT_VALUE = "update";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_ENVIRONMENT_VARIABLE = "DATABASE_HBM2DDL_AUTO";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_JAVA_OPTION = "DATABASE_HBM2DDL_AUTO";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_PROPERTY = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_PASSWORD_DEFAULT_VALUE = "postgres";

    @NotNull
    private static final String DATABASE_PASSWORD_ENVIRONMENT_VARIABLE = "DATABASE_PASSWORD";

    @NotNull
    private static final String DATABASE_PASSWORD_JAVA_OPTION = "DATABASE_PASSWORD";

    @NotNull
    private static final String DATABASE_PASSWORD_PROPERTY = "database.password";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT_VALUE = "true";

    @NotNull
    private static final String DATABASE_SHOW_SQL_ENVIRONMENT_VARIABLE = "DATABASE_SHOW_SQL";

    @NotNull
    private static final String DATABASE_SHOW_SQL_JAVA_OPTION = "DATABASE_SHOW_SQL";

    @NotNull
    private static final String DATABASE_SHOW_SQL_PROPERTY = "database.show_sql";

    @NotNull
    private static final String DATABASE_SQL_DIALECT_DEFAULT_VALUE = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String DATABASE_SQL_DIALECT_ENVIRONMENT_VARIABLE = "DATABASE_SQL_DIALECT";

    @NotNull
    private static final String DATABASE_SQL_DIALECT_JAVA_OPTION = "DATABASE_SQL_DIALECT";

    @NotNull
    private static final String DATABASE_SQL_DIALECT_PROPERTY = "database.sql_dialect";

    @NotNull
    private static final String DATABASE_URL_DEFAULT_VALUE = "jdbc:postgresql://127.0.0.1:5432/taskmanager";

    @NotNull
    private static final String DATABASE_URL_ENVIRONMENT_VARIABLE = "DATABASE_URL";

    @NotNull
    private static final String DATABASE_URL_JAVA_OPTION = "DATABASE_URL";

    @NotNull
    private static final String DATABASE_URL_PROPERTY = "database.url";

    @NotNull
    private static final String DATABASE_USERNAME_DEFAULT_VALUE = "postgres";

    @NotNull
    private static final String DATABASE_USERNAME_ENVIRONMENT_VARIABLE = "DATABASE_USERNAME";

    @NotNull
    private static final String DATABASE_USERNAME_JAVA_OPTION = "DATABASE_USERNAME";

    @NotNull
    private static final String DATABASE_USERNAME_PROPERTY = "database.username";

    @NotNull
    private static final String DATABASE_USE_SECOND_LVL_CACHE_DEFAULT_VALUE = "true";

    @NotNull
    private static final String DATABASE_USE_SECOND_LVL_CACHE_ENVIRONMENT_VARIABLE = "DATABASE_SECOND_LVL_CACHE";

    @NotNull
    private static final String DATABASE_USE_SECOND_LVL_CACHE_JAVA_OPTION = "DATABASE_SECOND_LVL_CACHE";

    @NotNull
    private static final String DATABASE_USE_SECOND_LVL_CACHE_PROPERTY = "database.cache.use_second_level_cache";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_DEFAULT_VALUE = "true";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_ENVIRONMENT_VARIABLE = "DATABASE_QUERY_CACHE";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_JAVA_OPTION = "DATABASE_QUERY_CACHE";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_PROPERTY = "database.cache.use_query_cache";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_DEFAULT_VALUE = "true";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_ENVIRONMENT_VARIABLE = "DATABASE_CACHE_USE_MINIMAL_PUTS";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_JAVA_OPTION = "DATABASE_CACHE_USE_MINIMAL_PUTS";

    @NotNull
    private static final String DATABASE_CACHE_USE_MINIMAL_PUTS_PROPERTY = "database.cache.use_minimal_puts";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_DEFAULT_VALUE = "tm";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_ENVIRONMENT_VARIABLE = "DATABASE_CACHE_REGION_PREFIX";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_JAVA_OPTION = "DATABASE_CACHE_REGION_PREFIX";

    @NotNull
    private static final String DATABASE_CACHE_REGION_PREFIX_PROPERTY = "database.cache.region_prefix";

    @NotNull
    private static final String DATABASE_CACHE_CONFIG_FILE_DEFAULT_VALUE = "hazelcast.xml";

    @NotNull
    private static final String DATABASE_CACHE_CONFIG_FILE_ENVIRONMENT_VARIABLE = "DATABASE_CACHE_CONFIG_FILE";

    @NotNull
    private static final String DATABASE_CACHE_CONFIG_FILE_JAVA_OPTION = "DATABASE_CACHE_CONFIG_FILE";

    @NotNull
    private static final String DATABASE_CACHE_CONFIG_FILE_PROPERTY = "database.cache.provider_configuration_file_resource_path";

    @NotNull
    private static final String DATABASE_CACHE_FACTORY_CLASS_DEFAULT_VALUE = "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory";

    @NotNull
    private static final String DATABASE_CACHE_FACTORY_CLASS_ENVIRONMENT_VARIABLE = "DATABASE_CACHE_FACTORY_CLASS";

    @NotNull
    private static final String DATABASE_CACHE_FACTORY_CLASS_JAVA_OPTION = "DATABASE_CACHE_FACTORY_CLASS";

    @NotNull
    private static final String DATABASE_CACHE_FACTORY_CLASS_PROPERTY = "database.cache.region.factory_class";

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String FILE_SCANNER_FREQUENCY_DEFAULT_VALUE = "5000";

    @NotNull
    private static final String FILE_SCANNER_FREQUENCY_ENVIRONMENT_VARIABLE = "FILE_SCANNER_FREQUENCY";

    @NotNull
    private static final String FILE_SCANNER_FREQUENCY_JAVA_OPTION = "FILE_SCANNER_FREQUENCY";

    @NotNull
    private static final String FILE_SCANNER_FREQUENCY_PROPERTY = "fileScanner.frequency";

    @NotNull
    private static final String FILE_SCANNER_PATH_DEFAULT_VALUE = "./";

    @NotNull
    private static final String FILE_SCANNER_PATH_ENVIRONMENT_VARIABLE = "FILE_SCANNER_PATH";

    @NotNull
    private static final String FILE_SCANNER_PATH_JAVA_OPTION = "FILE_SCANNER_PATH";

    @NotNull
    private static final String FILE_SCANNER_PATH_PROPERTY = "fileScanner.path";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT_VALUE = "0";

    @NotNull
    private static final String PASSWORD_ITERATION_ENVIRONMENT_VARIABLE = "PASSWORD_ITERATION";

    @NotNull
    private static final String PASSWORD_ITERATION_JAVA_OPTION = "PASSWORD_ITERATION";

    @NotNull
    private static final String PASSWORD_ITERATION_PROPERTY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT_VALUE = "";

    @NotNull
    private static final String PASSWORD_SECRET_ENVIRONMENT_VARIABLE = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_JAVA_OPTION = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_PROPERTY = "password.secret";

    @NotNull
    private static final String SIGN_ITERATION_DEFAULT_VALUE = "0";

    @NotNull
    private static final String SIGN_ITERATION_ENVIRONMENT_VARIABLE = "SESSION_SIGN_ITERATION";

    @NotNull
    private static final String SIGN_ITERATION_JAVA_OPTION = "SESSION_SIGN_ITERATION";

    @NotNull
    private static final String SIGN_ITERATION_PROPERTY = "session.sign.iteration";

    @NotNull
    private static final String SIGN_SECRET_DEFAULT_VALUE = "";

    @NotNull
    private static final String SIGN_SECRET_ENVIRONMENT_VARIABLE = "SESSION_SIGN_SECRET";

    @NotNull
    private static final String SIGN_SECRET_JAVA_OPTION = "SESSION_SIGN_SECRET";

    @NotNull
    private static final String SIGN_SECRET_PROPERTY = "session.sign.secret";

    @NotNull
    private final Properties properties = new Properties();

    public PropertyService() {
        loadPropertiesFromFile();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValue(APP_VERSION_JAVA_OPTION, APP_VERSION_ENVIRONMENT_VARIABLE, APP_VERSION_PROPERTY, APP_VERSION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getAutosaveFrequency() {
        return getValueInteger(AUTOSAVE_FREQUENCY_JAVA_OPTION,
                AUTOSAVE_FREQUENCY_ENVIRONMENT_VARIABLE,
                AUTOSAVE_FREQUENCY_PROPERTY,
                AUTOSAVE_FREQUENCY_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getValue(DATABASE_DRIVER_JAVA_OPTION,
                DATABASE_DRIVER_ENVIRONMENT_VARIABLE,
                DATABASE_DRIVER_PROPERTY,
                DATABASE_DRIVER_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseHbm2ddlAuto() {
        return getValue(DATABASE_HBM2DDL_AUTO_JAVA_OPTION,
                DATABASE_HBM2DDL_AUTO_ENVIRONMENT_VARIABLE,
                DATABASE_HBM2DDL_AUTO_PROPERTY,
                DATABASE_HBM2DDL_AUTO_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getValue(DATABASE_PASSWORD_JAVA_OPTION,
                DATABASE_PASSWORD_ENVIRONMENT_VARIABLE,
                DATABASE_PASSWORD_PROPERTY,
                DATABASE_PASSWORD_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getValue(DATABASE_SHOW_SQL_JAVA_OPTION,
                DATABASE_SHOW_SQL_ENVIRONMENT_VARIABLE,
                DATABASE_SHOW_SQL_PROPERTY,
                DATABASE_SHOW_SQL_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseSqlDialect() {
        return getValue(DATABASE_SQL_DIALECT_JAVA_OPTION,
                DATABASE_SQL_DIALECT_ENVIRONMENT_VARIABLE,
                DATABASE_SQL_DIALECT_PROPERTY,
                DATABASE_SQL_DIALECT_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getValue(DATABASE_URL_JAVA_OPTION,
                DATABASE_URL_ENVIRONMENT_VARIABLE,
                DATABASE_URL_PROPERTY,
                DATABASE_URL_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUsername() {
        return getValue(DATABASE_USERNAME_JAVA_OPTION,
                DATABASE_USERNAME_ENVIRONMENT_VARIABLE,
                DATABASE_USERNAME_PROPERTY,
                DATABASE_USERNAME_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUseSecondLvlCache() {
        return getValue(DATABASE_USE_SECOND_LVL_CACHE_JAVA_OPTION,
                DATABASE_USE_SECOND_LVL_CACHE_ENVIRONMENT_VARIABLE,
                DATABASE_USE_SECOND_LVL_CACHE_PROPERTY,
                DATABASE_USE_SECOND_LVL_CACHE_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUseQueryCache() {
        return getValue(DATABASE_USE_QUERY_CACHE_JAVA_OPTION,
                DATABASE_USE_QUERY_CACHE_ENVIRONMENT_VARIABLE,
                DATABASE_USE_QUERY_CACHE_PROPERTY,
                DATABASE_USE_QUERY_CACHE_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUseMinimalPuts() {
        return getValue(DATABASE_CACHE_USE_MINIMAL_PUTS_JAVA_OPTION,
                DATABASE_CACHE_USE_MINIMAL_PUTS_ENVIRONMENT_VARIABLE,
                DATABASE_CACHE_USE_MINIMAL_PUTS_PROPERTY,
                DATABASE_CACHE_USE_MINIMAL_PUTS_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseCacheRegionPrefix() {
        return getValue(DATABASE_CACHE_REGION_PREFIX_JAVA_OPTION,
                DATABASE_CACHE_REGION_PREFIX_ENVIRONMENT_VARIABLE,
                DATABASE_CACHE_REGION_PREFIX_PROPERTY,
                DATABASE_CACHE_REGION_PREFIX_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseCacheConfigFile() {
        return getValue(DATABASE_CACHE_CONFIG_FILE_JAVA_OPTION,
                DATABASE_CACHE_CONFIG_FILE_ENVIRONMENT_VARIABLE,
                DATABASE_CACHE_CONFIG_FILE_PROPERTY,
                DATABASE_CACHE_CONFIG_FILE_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseCacheFactoryClass() {
        return getValue(DATABASE_CACHE_FACTORY_CLASS_JAVA_OPTION,
                DATABASE_CACHE_FACTORY_CLASS_ENVIRONMENT_VARIABLE,
                DATABASE_CACHE_FACTORY_CLASS_PROPERTY,
                DATABASE_CACHE_FACTORY_CLASS_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getFileScannerFrequency() {
        return getValueInteger(FILE_SCANNER_FREQUENCY_JAVA_OPTION,
                FILE_SCANNER_FREQUENCY_ENVIRONMENT_VARIABLE,
                FILE_SCANNER_FREQUENCY_PROPERTY,
                FILE_SCANNER_FREQUENCY_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getFileScannerPath() {
        return getValue(FILE_SCANNER_PATH_JAVA_OPTION,
                FILE_SCANNER_PATH_ENVIRONMENT_VARIABLE,
                FILE_SCANNER_PATH_PROPERTY,
                FILE_SCANNER_PATH_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInteger(PASSWORD_ITERATION_JAVA_OPTION,
                PASSWORD_ITERATION_ENVIRONMENT_VARIABLE,
                PASSWORD_ITERATION_PROPERTY,
                PASSWORD_ITERATION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_JAVA_OPTION,
                PASSWORD_SECRET_ENVIRONMENT_VARIABLE,
                PASSWORD_SECRET_PROPERTY,
                PASSWORD_SECRET_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getPort() {
        return getValue(APP_PORT_JAVA_OPTION,
                APP_PORT_ENVIRONMENT_VARIABLE,
                APP_PORT_PROPERTY,
                APP_PORT_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getServer() {
        return getValue(APP_SERVER_JAVA_OPTION,
                APP_SERVER_ENVIRONMENT_VARIABLE,
                APP_SERVER_PROPERTY,
                APP_SERVER_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public Integer getSignIteration() {
        return getValueInteger(SIGN_ITERATION_JAVA_OPTION,
                SIGN_ITERATION_ENVIRONMENT_VARIABLE,
                SIGN_ITERATION_PROPERTY,
                SIGN_ITERATION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getSignSecret() {
        return getValue(SIGN_SECRET_JAVA_OPTION,
                SIGN_SECRET_ENVIRONMENT_VARIABLE,
                SIGN_SECRET_PROPERTY,
                SIGN_SECRET_DEFAULT_VALUE);
    }

    @NotNull
    private String getValue(@NotNull final String nameJavaOpts, @NotNull final String nameEnvironmentVariable,
                            @NotNull final String nameProperty, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(nameJavaOpts)) return System.getProperty(nameJavaOpts);
        if (System.getenv().containsKey(nameEnvironmentVariable)) return System.getenv(nameEnvironmentVariable);
        if (properties.containsKey(nameProperty)) return properties.getProperty(nameProperty);
        return defaultValue;
    }

    @NotNull
    private Boolean getValueBoolean(@NotNull final String nameJavaOpts, @NotNull final String nameEnvironmentVariable,
                                    @NotNull final String nameProperty, @NotNull final String defaultValue) {
        @NotNull final String value = getValue(nameJavaOpts, nameEnvironmentVariable, nameProperty, defaultValue);
        return Boolean.parseBoolean(value);
    }

    @NotNull
    private Integer getValueInteger(@NotNull final String nameJavaOpts, @NotNull final String nameEnvironmentVariable,
                                    @NotNull final String nameProperty, @NotNull final String defaultValue) {
        @NotNull final String value = getValue(nameJavaOpts, nameEnvironmentVariable, nameProperty, defaultValue);
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final NumberFormatException e) {
            return Integer.parseInt(PASSWORD_ITERATION_DEFAULT_VALUE);
        }
    }

    @NotNull
    @Override
    public Boolean isAutosaveOnExit() {
        return getValueBoolean(AUTOSAVE_ON_EXIT_JAVA_OPTION,
                AUTOSAVE_ON_EXIT_ENVIRONMENT_VARIABLE,
                AUTOSAVE_ON_EXIT_PROPERTY,
                AUTOSAVE_ON_EXIT_DEFAULT_VALUE);
    }

    @SneakyThrows
    private void loadPropertiesFromFile() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

}
