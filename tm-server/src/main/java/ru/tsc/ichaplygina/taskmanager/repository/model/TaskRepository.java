package ru.tsc.ichaplygina.taskmanager.repository.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class TaskRepository extends AbstractBusinessEntityRepository<Task> implements ITaskRepository {

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
    }

    @Override
    public void clearForUser(@NotNull String userId) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("FROM Task", Task.class).getResultList();
    }

    @Override
    public @NotNull List<Task> findAllByProjectId(@NotNull String projectId) {
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.project.id = :projectId", Task.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public @NotNull List<Task> findAllByProjectIdForUser(@NotNull String userId, @NotNull String projectId) {
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.project.id = :projectId e.user.id = :userId", Task.class)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllForUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable Task findById(@NotNull String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public @Nullable Task findByIdForUser(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("FROM Task e WHERE e.id = :id AND e.user.id = :userId", Task.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Task findByIndex(int index) {
        return entityManager
                .createQuery("FROM Task", Task.class)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Task findByIndexForUser(@NotNull String userId, int index) {
        return entityManager
                .createQuery("FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Task findByName(@NotNull String name) {
        return entityManager
                .createQuery("FROM Task e WHERE e.name = :name", Task.class)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Task findByNameForUser(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("FROM Task e WHERE e.user.id = :userId AND e.name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Task findTaskInProject(@NotNull String taskId, @NotNull String projectId) {
        return entityManager
                .createQuery("FROM Task e WHERE e.id = :taskId AND e.project.id = :projectId", Task.class)
                .setParameter("taskId", taskId)
                .setParameter("projectId", projectId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Task findTaskInProjectForUser(@NotNull String userId, @NotNull String taskId, @NotNull String projectId) {
        return entityManager
                .createQuery("FROM Task e WHERE e.id = :taskId AND e.project.id = :projectId AND e.user.id = :userId", Task.class)
                .setParameter("taskId", taskId)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByIndex(int index) {
        return entityManager
                .createQuery("SELECT id FROM Task", String.class)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByIndexForUser(@NotNull String userId, int index) {
        return entityManager
                .createQuery("SELECT id FROM Task e WHERE e.user.id = :userId", String.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByName(@NotNull String name) {
        return entityManager
                .createQuery("SELECT id FROM Task e WHERE e.name = :name", String.class)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByNameForUser(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("SELECT id FROM Task e WHERE e.user.id = :userId AND e.name = :name", String.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Task e", Long.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSizeForUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Task e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeAllByProjectId(@NotNull String projectId) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.project.id = :projectId")
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(entityManager.getReference(Task.class, id));
    }

    @Override
    public void removeByIdForUser(@NotNull String userId, @NotNull String id) {
        entityManager.createQuery("DELETE FROM Task e WHERE e.id = :id AND e.user.id = :userId")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(int index) {
        entityManager
                .createQuery("DELETE FROM Task")
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public void removeByIndexForUser(@NotNull String userId, int index) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public void removeByName(@NotNull String name) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.name = :name")
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByNameForUser(@NotNull String userId, @NotNull String name) {
        entityManager
                .createQuery("DELETE FROM Task e WHERE e.name = :name AND e.user.id = :userId")
                .setParameter("name", name)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
