package ru.tsc.ichaplygina.taskmanager.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.List;

public interface IAbstractBusinessEntityRepository<E extends AbstractBusinessEntity> extends IAbstractRepository<E> {

    void clearForUser(@NotNull String currentUserId);

    @NotNull
    List<E> findAllForUser(@NotNull String userId);

    @Nullable
    E findByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    E findByIndex(int index);

    @Nullable
    E findByIndexForUser(@NotNull String userId, int index);

    @Nullable
    E findByName(@NotNull String name);

    @Nullable
    E findByNameForUser(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByIndex(int index);

    @Nullable
    String getIdByIndexForUser(@NotNull String userId, int index);

    @Nullable
    String getIdByName(@NotNull String name);

    @Nullable
    String getIdByNameForUser(@NotNull String userId, String name);

    long getSize();

    long getSizeForUser(@NotNull String userId);

    void removeByIdForUser(@NotNull String userId, @NotNull String id);

    void removeByIndex(int index);

    void removeByIndexForUser(@NotNull String userId, int index);

    void removeByName(@NotNull String name);

    void removeByNameForUser(@NotNull String userId, @NotNull String name);

}
