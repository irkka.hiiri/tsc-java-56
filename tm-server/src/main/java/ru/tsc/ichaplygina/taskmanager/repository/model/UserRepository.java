package ru.tsc.ichaplygina.taskmanager.repository.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void clear() {
        for (@NotNull final User user : findAll()) entityManager.remove(user);
    }

    @Override
    public @NotNull List<User> findAll() {
        return entityManager.createQuery("FROM User", User.class).getResultList();
    }

    @Override
    public @Nullable User findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("FROM User e WHERE e.email = :email", User.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable User findById(@NotNull String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public @Nullable User findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String findIdByEmail(@NotNull String email) {
        return entityManager
                .createQuery("SELECT id FROM User e WHERE e.email = :email", String.class)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String findIdByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT id FROM User e WHERE e.login = :login", String.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM User e", Long.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(entityManager.getReference(User.class, id));
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        entityManager.remove(findByLogin(login));
    }

}
