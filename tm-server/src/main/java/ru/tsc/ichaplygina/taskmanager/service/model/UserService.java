package ru.tsc.ichaplygina.taskmanager.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.EmailEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithEmailException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithLoginException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    public IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final String login, @NotNull final String password, @NotNull final String email, @NotNull final Role role,
                          @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @NotNull final User user = new User(login, Objects.requireNonNull(salt(password, propertyService)), email, firstName, middleName, lastName, role);
        add(user);
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final User user) {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (!isEmptyString(repository.findIdByLogin(user.getLogin())))
                throw new UserExistsWithLoginException(user.getLogin());
            if (!isEmptyString(repository.findIdByEmail(user.getEmail())))
                throw new UserExistsWithEmailException(user.getEmail());
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@Nullable List<User> users) {
        if (users == null) return;
        for (final User user : users) add(user);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public final User findByLogin(@NotNull final String login) {
        return Optional.ofNullable(findUserByLogin(login)).orElseThrow(UserNotFoundException::new);
    }

    @Nullable
    @Override
    public final User findByLoginForAuthorization(@NotNull final String login) {
        return Optional.ofNullable(findUserByLogin(login)).orElseThrow(IncorrectCredentialsException::new);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String findIdByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findIdByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    private User findUserByLogin(@NotNull final String login) {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return (getSize() == 0);
    }

    @Override
    public final boolean isPrivilegedUser(@NotNull final String userId) {
        @NotNull final User user = Optional.ofNullable(findById(userId)).orElseThrow(UserNotFoundException::new);
        return user.getRole().equals(Role.ADMIN);
    }

    @Override
    @SneakyThrows
    public final boolean lockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final User user = Optional.ofNullable(repository.findById(id)).orElseThrow(UserNotFoundException::new);
            if (user.isLocked()) return false;
            user.setLocked(true);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return true;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean lockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final User user = Optional.ofNullable(repository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            if (user.isLocked()) return false;
            user.setLocked(true);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return true;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final User user = Optional.ofNullable(repository.findById(id)).orElseThrow(UserNotFoundException::new);
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public final User removeByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new IdEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final User user = Optional.ofNullable(repository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            entityManager.getTransaction().begin();
            repository.removeByLogin(login);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void setPassword(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
            user.setPasswordHash(salt(password, propertyService));
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void setRole(@NotNull final String login, @NotNull final Role role) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
            user.setRole(role);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean unlockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final User user = Optional.ofNullable(repository.findById(id)).orElseThrow(UserNotFoundException::new);
            if (!user.isLocked()) return false;
            user.setLocked(false);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return true;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean unlockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final User user = Optional.ofNullable(repository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            if (!user.isLocked()) return false;
            user.setLocked(false);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return true;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final User updateById(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final String email,
                                 @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final User userFoundWithThisLogin = repository.findByLogin(login);
            @Nullable final User userFoundWithThisEmail = repository.findByEmail(email);
            if (userFoundWithThisLogin != null && !id.equals(userFoundWithThisLogin.getId()))
                throw new UserExistsWithLoginException(login);
            if (userFoundWithThisEmail != null && !id.equals(userFoundWithThisEmail.getId()))
                throw new UserExistsWithEmailException(email);
            @NotNull final User user = Optional.ofNullable(findById(id)).orElseThrow(UserNotFoundException::new);
            user.setLogin(login);
            user.setPasswordHash(password);
            user.setEmail(email);
            user.setRole(role);
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final User updateByLogin(@NotNull final String login, @NotNull final String password, @NotNull final String email,
                                    @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return updateById(id, login, password, email, role, firstName, middleName, lastName);
    }

}
