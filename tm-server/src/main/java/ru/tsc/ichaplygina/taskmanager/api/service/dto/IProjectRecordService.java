package ru.tsc.ichaplygina.taskmanager.api.service.dto;

import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;

public interface IProjectRecordService extends IAbstractBusinessEntityRecordService<ProjectDTO> {


}
