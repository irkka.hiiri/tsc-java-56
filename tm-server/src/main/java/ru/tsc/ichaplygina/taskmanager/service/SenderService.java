package ru.tsc.ichaplygina.taskmanager.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.service.ISenderService;
import ru.tsc.ichaplygina.taskmanager.dto.EntityLogDTO;

import javax.jms.*;
import java.util.Date;

@Service
public class SenderService implements ISenderService {

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);

    @Override
    @SneakyThrows
    public void send(@NotNull final EntityLogDTO entity) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic("JCG_TOPIC");
        final MessageProducer producer = session.createProducer(destination);
        final ObjectMessage message = session.createObjectMessage(entity);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

    @Override
    @SneakyThrows
    public EntityLogDTO createMessage(@NotNull final Object object, @NotNull final String type) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        @NotNull final String className = object.getClass().getSimpleName();
        @NotNull final EntityLogDTO message = new EntityLogDTO(className, new Date().toString(), type, json);
        return message;
    }


}
