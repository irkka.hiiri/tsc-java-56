package ru.tsc.ichaplygina.taskmanager.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.List;

public interface IAbstractService<E extends AbstractModel> {

    void add(@NotNull final E entity);

    void addAll(@Nullable List<E> entities);

    void clear();

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    long getSize();

    boolean isEmpty();

    @Nullable
    E removeById(@NotNull String id);

}
