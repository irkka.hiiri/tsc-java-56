package ru.tsc.ichaplygina.taskmanager.repository.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.ISessionRepository;
import ru.tsc.ichaplygina.taskmanager.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Session e").executeUpdate();
    }

    @Override
    public @NotNull List<Session> findAll() {
        return entityManager.createQuery("FROM Session", Session.class).getResultList();
    }

    @Override
    public @Nullable Session findById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(1) FROM Session e", Long.class).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(Session.class, id));
    }

}
