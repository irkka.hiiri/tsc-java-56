package ru.tsc.ichaplygina.taskmanager.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.api.repository.model.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class ProjectRepository extends AbstractBusinessEntityRepository<Project> implements IProjectRepository {

    @Override
    public void clear() {
        for (@NotNull final Project project : findAll()) entityManager.remove(project);
    }

    @Override
    public void clearForUser(@NotNull String userId) {
        for (@NotNull final Project project : findAllForUser(userId)) entityManager.remove(project);
    }

    @Override
    public @NotNull List<Project> findAll() {
        return entityManager.createQuery("FROM Project", Project.class).getResultList();
    }

    @Override
    public @NotNull List<Project> findAllForUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable Project findById(@NotNull String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public @Nullable Project findByIdForUser(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("FROM Project e WHERE e.id = :id AND e.user.id = :userId", Project.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Project findByIndex(int index) {
        return entityManager
                .createQuery("FROM Project", Project.class)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Project findByIndexForUser(@NotNull String userId, int index) {
        return entityManager
                .createQuery("FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Project findByName(@NotNull String name) {
        return entityManager
                .createQuery("FROM Project e WHERE e.name = :name", Project.class)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable Project findByNameForUser(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("FROM Project e WHERE e.user.id = :userId AND e.name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByIndex(int index) {
        return entityManager
                .createQuery("SELECT id FROM Project", String.class)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByIndexForUser(@NotNull String userId, int index) {
        return entityManager
                .createQuery("SELECT id FROM Project e WHERE e.user.id = :userId", String.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByName(@NotNull String name) {
        return entityManager
                .createQuery("SELECT id FROM Project e WHERE e.name = :name", String.class)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable String getIdByNameForUser(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("SELECT id FROM Project e WHERE e.user.id = :userId AND e.name = :name", String.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Project e", Long.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSizeForUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Project e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(entityManager.getReference(Project.class, id));
    }

    @Override
    public void removeByIdForUser(@NotNull String userId, @NotNull String id) {
        entityManager.remove(findByIdForUser(userId, id));
    }

    @Override
    public void removeByIndex(int index) {
        entityManager.remove(findByIndex(index));
    }

    @Override
    public void removeByIndexForUser(@NotNull String userId, int index) {
        entityManager.remove(findByIndexForUser(userId, index));
    }

    @Override
    public void removeByName(@NotNull String name) {
        entityManager.remove(findByName(name));
    }

    @Override
    public void removeByNameForUser(@NotNull String userId, @NotNull String name) {
        entityManager.remove(getIdByNameForUser(userId, name));
    }

}
