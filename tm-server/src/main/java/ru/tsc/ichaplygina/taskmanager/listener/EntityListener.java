package ru.tsc.ichaplygina.taskmanager.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.component.Bootstrap;
import ru.tsc.ichaplygina.taskmanager.component.MessageExecutor;
import ru.tsc.ichaplygina.taskmanager.enumerated.EntityOperationType;

import javax.persistence.*;

import static ru.tsc.ichaplygina.taskmanager.enumerated.EntityOperationType.*;

@Component
public class EntityListener {

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        sendMessage(entity, POST_LOAD);
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity) {
        sendMessage(entity, PRE_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        sendMessage(entity, POST_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity) {
        sendMessage(entity, PRE_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        sendMessage(entity, POST_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity) {
        sendMessage(entity, PRE_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, POST_UPDATE);
    }

    @SneakyThrows
    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        //Bootstrap.sendMessage(entity, operationType.toString());
        MessageExecutor.getInstance().sendMessage(entity, operationType.toString());
    }

}
