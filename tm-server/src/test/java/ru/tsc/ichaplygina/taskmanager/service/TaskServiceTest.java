package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.ITaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;
import ru.tsc.ichaplygina.taskmanager.configuration.ServerConfiguration;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.marker.DatabaseCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.User;
import ru.tsc.ichaplygina.taskmanager.service.model.ProjectService;
import ru.tsc.ichaplygina.taskmanager.service.model.ProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.service.model.TaskService;
import ru.tsc.ichaplygina.taskmanager.service.model.UserService;

import java.util.ArrayList;
import java.util.List;

public class TaskServiceTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static ITaskService taskService;

    @NotNull
    private static IProjectService projectService;

    @NotNull
    private static IUserService userService;

    @NotNull
    private static BrokerService brokerService;

    @NotNull
    private List<Task> taskList;

    @After
    public void clean() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        taskService.clear(userId);
        projectService.clear(userId);
        for (@NotNull final Task task : taskService.findAll(adminUserId)) {
            if (adminUserId.equals(task.getUser().getId()))
                taskService.removeById(adminUserId, task.getId());
        }
        for (@NotNull final Project project : projectService.findAll(adminUserId)) {
            if (adminUserId.equals(project.getUser().getId()))
                taskService.removeById(adminUserId, project.getId());
        }
        userService.removeByLogin("user");
        userService.removeByLogin("admin");
    }

    @BeforeClass
    public static void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        propertyService = context.getBean(PropertyService.class);
        projectService = context.getBean(ProjectService.class);
        taskService = context.getBean(TaskService.class);
        userService = context.getBean(UserService.class);
        initJMS();
    }

    @SneakyThrows
    @AfterClass
    public static void close() {
        brokerService.stop();
    }

    @SneakyThrows
    private static void initJMS() {
        BasicConfigurator.configure();
        brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
    }

    @Before
    public void initTest() {
        taskList = new ArrayList<>();
        @NotNull final User admin = new User("admin", "admin", "admin@admin", "A.", "D.", "Min", Role.ADMIN);
        @NotNull final User user = new User("user", "user", "user@user", "U.", "S.", "Er", Role.USER);
        userService.add(admin);
        userService.add(user);
        taskList.add(new Task("Admin Task 1", "", admin));
        taskList.add(new Task("Admin Task 2", "", admin));
        taskList.add(new Task("User Task 1", "", user));
        taskList.add(new Task("User Task 2", "", user));
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAdd() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertEquals(4, taskService.getSize());
        taskService.add(userId, "123", "123");
        Assert.assertEquals(5, taskService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAddAll() {
        @NotNull List<Task> taskList = new ArrayList<>();
        @NotNull final User user = userService.findByLogin("user");
        for (int i = 1; i <= 10; i++) {
            taskList.add(new Task("task add" + i, "", user));
        }
        taskService.addAll(taskList);
        Assert.assertEquals(14, taskService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAddTaskToProject() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectService.add(userId, "test project add task in project", "");
        Assert.assertNotNull(taskService.addTaskToProject(userId, taskList.get(0).getId(), projectService.findAll().get(0).getId()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAddTaskToProjectUnknownTask() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNull(taskService.addTaskToProject(userId, "123", "someProjectId"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAddTaskToProjectWrongUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertNull(taskService.addTaskToProject(userId, taskList.get(0).getId(), "someProjectId"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testClearAdmin() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String adminUserId = userService.findByLogin("user").getId();
        taskService.clear(adminUserId);
        Assert.assertEquals(0, taskService.getSize(adminUserId));
        Assert.assertEquals(0, taskService.getSize(userId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testClearUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        taskService.clear(userId);
        Assert.assertEquals(0, taskService.getSize(userId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(taskService.completeById(userId, taskList.get(0).getId()));
        Assert.assertEquals(Status.COMPLETED, taskService.findById(userId, taskList.get(0).getId()).getStatus());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteByIdWrongUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertNull(taskService.completeById(userId, taskList.get(0).getId()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(taskService.completeByName(userId, taskList.get(0).getName()));
        Assert.assertEquals(Status.COMPLETED, taskService.findById(userId, taskList.get(0).getId()).getStatus());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCompleteByNameWrongUser() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNull(taskService.completeById(userId, taskList.get(0).getName()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull List<Task> taskList = taskService.findAll(userId);
        Assert.assertEquals(taskService.getSize(userId), taskList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllByProjectId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectService.add(userId, "test project find task in project", "");
        projectService.add(userId, "test project find task in project 1", "");
        @NotNull final String projectId1 = projectService.findAll().get(0).getId();
        @NotNull final String projectId2 = projectService.findAll().get(1).getId();
        taskService.addTaskToProject(userId, taskList.get(0).getId(), projectId1);
        taskService.addTaskToProject(userId, taskList.get(1).getId(), projectId1);
        taskService.addTaskToProject(userId, taskList.get(2).getId(), projectId2);
        taskService.addTaskToProject(userId, taskList.get(3).getId(), projectId2);
        Assert.assertEquals(2, taskService.findAllByProjectId(userId, projectId2, "").size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAllUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull List<Task> taskList = taskService.findAll(userId);
        Assert.assertEquals(2, taskList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertNotNull(taskService.findById(userId, taskId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertNull(taskService.findById(userId, taskId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByNameAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskName = "Admin Task 1";
        Assert.assertEquals(taskList.get(0).getId(), taskService.findByName(userId, taskName).getId());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByNameUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String taskName = "Admin Task 1";
        Assert.assertNull(taskService.findByName(userId, taskName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetIdByNameAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskName = "Admin Task 1";
        Assert.assertEquals(taskList.get(0).getId(), taskService.getId(userId, taskName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetIdByNameUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String taskName = "Admin Task 1";
        Assert.assertNull(taskService.getId(userId, taskName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSizeAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertEquals(4, taskService.getSize(userId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSizeUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertEquals(2, taskService.getSize(userId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmptyAdmin() {
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        @NotNull final String userUserId = userService.findByLogin("user").getId();
        Assert.assertFalse(taskService.isEmpty(adminUserId));
        taskService.clear(adminUserId);
        Assert.assertTrue(taskService.isEmpty(adminUserId));
        Assert.assertTrue(taskService.isEmpty(userUserId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmptyUser() {
        @NotNull final String adminUserId = userService.findByLogin("admin").getId();
        @NotNull final String userUserId = userService.findByLogin("user").getId();
        Assert.assertFalse(taskService.isEmpty(userUserId));
        taskService.clear(userUserId);
        Assert.assertTrue(taskService.isEmpty(userUserId));
        Assert.assertFalse(taskService.isEmpty(adminUserId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveAllByProjectId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectService.add(userId, "test project find task in project", "");
        projectService.add(userId, "test project find task in project 1", "");
        @NotNull final String projectId1 = projectService.findAll().get(0).getId();
        @NotNull final String projectId2 = projectService.findAll().get(1).getId();
        taskService.addTaskToProject(userId, taskList.get(0).getId(), projectId1);
        taskService.addTaskToProject(userId, taskList.get(1).getId(), projectId1);
        taskService.addTaskToProject(userId, taskList.get(2).getId(), projectId2);
        taskService.addTaskToProject(userId, taskList.get(3).getId(), projectId2);
        taskService.removeAllByProjectId(projectId2);
        Assert.assertEquals(2, taskService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveByIdUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertNull(taskService.removeById(userId, taskId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveByNameAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskName = "Admin Task 1";
        Assert.assertNotNull(taskService.removeByName(userId, taskName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveByNameUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        @NotNull final String taskName = "Admin Task 1";
        Assert.assertNull(taskService.removeByName(userId, taskName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveTaskFromProject() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        projectService.add(userId, "test project add task in project", "");
        @NotNull final String projectId = projectService.findAll().get(0).getId();
        taskService.addTaskToProject(userId, taskList.get(0).getId(), projectId);
        Assert.assertNotNull(taskService.removeTaskFromProject(userId, taskList.get(0).getId(), projectId));
    }

    @Test(expected = TaskNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveTaskFromProjectUnknownTask() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        taskService.removeTaskFromProject(userId, "123", "someProjectId");
    }

    @Test(expected = TaskNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveTaskFromProjectWrongProject() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        taskService.removeTaskFromProject(userId, taskList.get(0).getId(), "someProjectId");
    }

    @Test(expected = TaskNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveTaskFromProjectWrongUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        taskService.removeTaskFromProject(userId, taskList.get(0).getId(), "someProjectId");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemovedByIdAdmin() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertNotNull(taskService.removeById(userId, taskId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testStartById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(taskService.startById(userId, taskList.get(0).getId()));
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findById(taskList.get(0).getId()).getStatus());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testStartByIdWrongUser() {
        @NotNull final String userId = userService.findByLogin("user").getId();
        Assert.assertNull(taskService.startById(userId, taskList.get(0).getId()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testStartByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNotNull(taskService.startByName(userId, taskList.get(0).getName()));
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findById(taskList.get(0).getId()).getStatus());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testStartByNameWrongUser() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        Assert.assertNull(taskService.startById(userId, taskList.get(0).getName()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertNotNull(taskService.updateById(userId, taskId, "new name", "new description"));
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdEmptyId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = "";
        taskService.updateById(userId, taskId, "new name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdEmptyName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        taskService.updateById(userId, taskId, "", "new description");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateByIdUnknownId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = "???";
        Assert.assertNull(taskService.updateById(userId, taskId, "new name", "new description"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateStatusById() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = taskList.get(0).getId();
        Assert.assertNotNull(taskService.completeById(userId, taskId));
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateStatusByIdEmptyId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = "";
        Assert.assertNotNull(taskService.completeById(userId, taskId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateStatusByIdUnknownId() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskId = "???";
        Assert.assertNull(taskService.completeById(userId, taskId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateStatusByName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskName = taskList.get(0).getName();
        Assert.assertNotNull(taskService.completeByName(userId, taskName));
    }

    @Test(expected = NameEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateStatusByNameEmptyName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskName = "";
        Assert.assertNotNull(taskService.completeByName(userId, taskName));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateStatusByNameUnknownName() {
        @NotNull final String userId = userService.findByLogin("admin").getId();
        @NotNull final String taskName = "???";
        Assert.assertNull(taskService.completeByName(userId, taskName));
    }

}
