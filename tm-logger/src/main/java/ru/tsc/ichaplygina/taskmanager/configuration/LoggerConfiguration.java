package ru.tsc.ichaplygina.taskmanager.configuration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.ichaplygina.taskmanager.listener.LoggerListener;

import javax.jms.ConnectionFactory;
import javax.jms.MessageListener;

@Configuration
@ComponentScan("ru.tsc.ichaplygina.taskmanager")
public class LoggerConfiguration {

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        return factory;
    }

    @Bean
    public MessageListener listener() {
        return new LoggerListener();
    }

}
